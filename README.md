# Weather Forecast



## Gyalpozhing College of Information Technology (Bachelor of Science of Information Technology ) PRJ303
Weather prediction is a prediction of weather by using parameters such as precipitation, maximum temperature, minimum temperature and wind. The Ridge machine learning algorithm is used to forecast the weather. 

Visit our website : (https://weather-prediction-prj.herokuapp.com)

Built using: Python, HTML/CSS/JS, Flask

Promotional video : (https://www.youtube.com/watch?v=qdfX8yOJ0CM)

## Screenshot of Website 
<img src="screenshots/home.png"/>
<img src="screenshots/portfolio.png"/>
<img src="screenshots/about.png"/>
<img src="screenshots/team.png"/>
<img src="screenshots/footer.png"/>

### PREDICT
Sample parameters 

fog( Precipitation: 0, Maximum temperature: 1.1, Minimum temperature: -1.1, Wind: 0)

rain( Precipitation: 3, Maximum temperature: 10.2, Minimum temperature: 3.7, Wind: 4)

snow( Precipitation: 5.3, Maximum temperature: 1.1, Minimum temperature: -3.3, Wind: 2.2)

sun( Precipitation: 0, Maximum temperature: 14.4, Minimum temperature: 2.2, Wind: 5.3)



<img src="screenshots/predict.png"/>



## Logo
<img src="screenshots/logo.png"/>

##  Final Poster 
<img src="screenshots/poster.png"/>


